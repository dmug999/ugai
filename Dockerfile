FROM crystallang/crystal:0.34.0-alpine AS builder

WORKDIR /Mango

COPY . .
COPY package*.json .
RUN apk add --no-cache yarn yaml sqlite-static \
    && make static

FROM alpine:3.7

WORKDIR /app

COPY --from=builder /Mango/mango .

RUN apk add --no-cache nano bash

CMD ["./mango"]
